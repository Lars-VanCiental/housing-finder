# Housing Finder



## Goal

The housing finder project is a small tool aiming at easing the process of finding a house.

It works by parsing housing ads websites, suggest them to the user and allowing follow up and visit scheduling on each ad.


## Configuration

You will find a sample `app/bootstrap.dist.php` with example on how to configure the housing finder project.


## Cli

 * **app/cli-config.php** is a command to use Doctrine orm cli tools.
 * **app/cli-import.php** is a command to import housing ads (50 for each source) based on the configuration found in your `app/bootstrap.php`.
 * **app/cli-image.php** is a command to clean old images.


## The website

The application is mainly splitted in three part.

The first part is about exploring housing ads (`/ads/browse`). You review housing ads one by one, either by accepting or rejecting them.
You will also be warned of duplicates.

When running out of housing ads, you will be redirected to the second part, where you can import (`/ads/import`) either one specific housind ad, or import (lightly) some new one based on your configuration.
Be careful since this automated process can take up quite some time. 

The last part is the watching part (`/ads/list`), where you find a list of all accepted housing ads and where you can review each one of them.
From the list you can access the details of any given housing ad (`/ads/{source-id}_{housing-ad-id}/show`).


## To do

Here is a list of todo, feel free to submit a pull-request :D

 * Deprecated as separated attribute.
 * Visit as real entity.
 * Display calendar with visits.
 * Improve Matcher.
 * Add global scoring based on matcher.
 * Improve duplicate finder.
 * Improve cli importer.
 * Improve Importer.
 * Adding multi process import.
 * Translate views.
 * Ability to dissociate duplicates. 