<?php

require_once __DIR__ . '/../app/bootstrap.php';

$app = new Silex\Application($app);

// Twig configuration
$app->register(
    new Silex\Provider\TwigServiceProvider(),
    [
        'twig.path' => __DIR__.'/../src/HousingFinder/Application/views',
    ]
);

if (empty($app['images.directory.housing_ad'])) {
    $app['images.directory.housing_ad'] = __DIR__.'/../web/images/housing_ads';
}

$app->mount('/', new \HousingFinder\Application\Controller\DefaultController($app));
$app->mount('/ads', new \HousingFinder\Application\Controller\HousingAdController($app));

$app->run();
