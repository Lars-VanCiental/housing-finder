<?php

require_once 'bootstrap.php';

$housingAdImporter = new \HousingFinder\Domain\Service\HousingAd\Importer(
    $app['orm.entity_manager']->getRepository(\HousingFinder\Domain\Model\Entity\HousingAd::class),
    $app['housing_finder']['housing_add']['sources'],
    $app['housing_finder']['housing_add']['matchers'],
    $app['images.manager']
);
$importReport = $housingAdImporter->importNewHousingAd(50);
$app['orm.entity_manager']->flush();

echo $importReport->getNumberOfHousingAdSourceConsulted().' sources consulted.'.PHP_EOL;
echo $importReport->getNumberOfHousingAdParsed().' housing ads parsed.'.PHP_EOL;
echo $importReport->getNumberOfHousingAdAdded().' housing ads added.'.PHP_EOL;
