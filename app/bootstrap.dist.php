<?php

require_once __DIR__ . '/../vendor/autoload.php';

// Base configuration.
$app = [];

// Directory configuration.
$app['images.directory.housing_ad'] = __DIR__.'/../web/images';
$app['images.manager'] = new \HousingFinder\Domain\Service\HousingAd\ImageManager($app['images.directory.housing_ad']);

// Doctrine configuration.
$yamlDriver = new \Doctrine\ORM\Mapping\Driver\SimplifiedYamlDriver(
    [
        $app['dir.root'].'/src/HousingFinder/Architecture/resources/doctrine' => 'HousingFinder\Domain\Model',
    ],
    '.yml'
);
$config = \Doctrine\ORM\Tools\Setup::createConfiguration($app['debug']);
$config->setMetadataDriverImpl($yamlDriver);
$app['orm.entity_manager'] = \Doctrine\ORM\EntityManager::create(
    \Doctrine\DBAL\DriverManager::getConnection(
        [
            'driver' => 'pdo_sqlite',
            'path'   => __DIR__.'/db.sqlite',
        ],
        new \Doctrine\DBAL\Configuration()
    ),
    $config
);

// Application configuration.
$app['housing_finder'] = [
    'housing_add' => [
        'sources' => [
            new \HousingFinder\Domain\Service\HousingAd\Source\LeBonCoin(
                'le-bon-coin',
                'http://www.leboncoin.fr/your-query/with-custom-parameters',
                '&o=',
                $app['images.manager']
            ),
        ],
        'matchers' => [
            new \HousingFinder\Domain\Service\HousingAd\Matcher\CaseInsensitiveMaxScore(
                new \HousingFinder\Domain\Model\ValueObject\Pattern(
                    'balcon',
                    'Balcon',
                    [
                        'balcon' => 1,
                        'loggia' => 2,
                        'terrasse' => 3,
                    ]
                ),
                new \HousingFinder\Domain\Model\ValueObject\Pattern(
                    'parking',
                    'Parking',
                    [
                        'stationnement' => 1,
                        'parking' => 2,
                        'garage' => 3,
                    ]
                )
            ),
        ],
    ],
];
