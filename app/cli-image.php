<?php

require_once 'bootstrap.php';

/** @var \HousingFinder\Architecture\DoctrineRepository\HousingAdRepository $housingAdRepository */
$housingAdRepository = $app['orm.entity_manager']->getRepository(\HousingFinder\Domain\Model\Entity\HousingAd::class);
/** @var HousingFinder\Domain\Service\HousingAd\Source\SourceInterface[] $housingAdSources */
$housingAdSources = array_combine(
    array_map(
        function (HousingFinder\Domain\Service\HousingAd\Source\SourceInterface $housingAdSource) {
            return $housingAdSource->getName();
        },
        $app['housing_finder']['housing_add']['sources']
    ),
    $app['housing_finder']['housing_add']['sources']
);

$removeDirectory = function ($directoryPath) {
    $numberOfFilesRemoved = 0;

    $directoryToRemove = new RecursiveDirectoryIterator($directoryPath, RecursiveDirectoryIterator::SKIP_DOTS);
    $filesToRemove = new RecursiveIteratorIterator($directoryToRemove, RecursiveIteratorIterator::CHILD_FIRST);
    foreach ($filesToRemove as $fileToRemove) {
        if ($fileToRemove->isDir()) {
            rmdir($fileToRemove->getRealPath());
        } else {
            unlink($fileToRemove->getRealPath());
            $numberOfFilesRemoved++;
        }
    }
    rmdir($directoryPath);

    return $numberOfFilesRemoved;
};

// Clear saved images of rejected housing_ad.
foreach ($housingAdRepository->findAll() as $housingAd) {
    if (($housingAd->getStatus() == \HousingFinder\Domain\Model\Entity\HousingAd::STATUS_REJECTED) || ($housingAd->getOriginal() != null) && ($housingAd->getOriginal()->getStatus() == \HousingFinder\Domain\Model\Entity\HousingAd::STATUS_REJECTED)) {
        $housingAd->setImages([]);
    }
}
$app['orm.entity_manager']->flush();

// Check existing images and remove outdated.
$numberOfUnknownSources = 0;
$numberOfRejectedHousingAdImagesRemoved = 0;
$numberOfRejectedHousingAdDirectoriesRemoved = 0;
$numberOfHousingAdImagesUpdated = 0;
$numberOfHousingAdImagesDeprecated = 0;
$imagesDirectory = new RecursiveDirectoryIterator(
    $app['images.directory.housing_ad'],
    RecursiveDirectoryIterator::SKIP_DOTS
);
$filesTCheck = new RecursiveIteratorIterator($imagesDirectory, RecursiveIteratorIterator::CHILD_FIRST);
foreach ($filesTCheck as $fileToCheck) {
    if ($fileToCheck->isDir()) {
        $source = null;
        $sourceIdentifier = null;
        /** @var \HousingFinder\Domain\Service\HousingAd\Source\SourceInterface $housingAdSource */
        if (isset($housingAdSources[basename($fileToCheck)])) {
            continue;
        }
        // Current directory is an housing_ad inside a source directory.
        if (isset($housingAdSources[basename(dirname($fileToCheck))])) {
            $source = $housingAdSources[basename(dirname($fileToCheck))];
            $sourceIdentifier = basename($fileToCheck);
        }

        echo $fileToCheck.PHP_EOL;

        // Directory done, skip to next.
        if (null == $source) {
            $numberOfUnknownSources++;
            continue;
        }

        // Getting housing ad.
        $housingAd = null;
        if (null !== $sourceIdentifier) {
            $housingAdIdentifier = new \HousingFinder\Domain\Model\Identifier\HousingAdIdentifier(
                $source->getName(),
                $sourceIdentifier
            );
            try {
                $housingAd = $housingAdRepository->get($housingAdIdentifier);
            } catch (\Doctrine\ORM\EntityNotFoundException $e) {
            }
        }

        if ((null == $housingAd)
            || ($housingAd->getStatus() == \HousingFinder\Domain\Model\Entity\HousingAd::STATUS_REJECTED)
            || (
                ($housingAd->getOriginal() != null)
                && ($housingAd->getOriginal()->getStatus() == \HousingFinder\Domain\Model\Entity\HousingAd::STATUS_REJECTED)
            )
        ) {
            $numberOfRejectedHousingAdImagesRemoved += $removeDirectory($fileToCheck->getRealPath());
            $numberOfRejectedHousingAdDirectoriesRemoved++;
            if (null != $housingAd) {
                $app['orm.entity_manager']->flush();
            }
            continue;
        }
    }
}

echo '======== Report ========'.PHP_EOL;
echo ' - '.$numberOfUnknownSources.' unknown sources.'.PHP_EOL;
echo ' - '.$numberOfRejectedHousingAdImagesRemoved.' rejected housing ad images removed.'.PHP_EOL;
echo ' - '.$numberOfRejectedHousingAdDirectoriesRemoved.' rejected housing ad directories removed.'.PHP_EOL;
echo ' - '.$numberOfHousingAdImagesUpdated.' housing ad images updated.'.PHP_EOL;
echo ' - '.$numberOfHousingAdImagesDeprecated.' housing ad images deprecated.'.PHP_EOL;
