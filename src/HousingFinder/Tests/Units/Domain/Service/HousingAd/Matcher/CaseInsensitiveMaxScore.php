<?php

namespace HousingFinder\Tests\Units\Domain\Service\HousingAd\Matcher;

use atoum;
use HousingFinder\Domain\Model\ValueObject\Match;
use HousingFinder\Domain\Model\ValueObject\Pattern;

class CaseInsensitiveMaxScore extends atoum
{
    public function testGetMatch()
    {
        $this
            ->given(
                $text = 'Some test with keywordA, keywordB and some more keywordB and Keywordb',
                $pattern = new Pattern('test', 'Test', ['keywordA' => 1, 'keywordB' => 1])
            )
            ->object($match = \HousingFinder\Domain\Service\HousingAd\Matcher\CaseInsensitiveMaxScore::getMatch($text, $pattern))
                ->isInstanceOf(Match::class)
            ->object($match->getPattern())
                ->isIdenticalTo($pattern)
            ->float($match->getScore())
                ->isEqualTo(4)
        ;
    }

    public function testGetMatchWithWeights()
    {
        $this
            ->given(
                $text = 'Some test with keywordA, keywordB and some more keywordB Keywordb',
                $pattern = new Pattern('test', 'Test', ['keywordA' => 1, 'keywordB' => 2])
            )
            ->object($match = \HousingFinder\Domain\Service\HousingAd\Matcher\CaseInsensitiveMaxScore::getMatch($text, $pattern))
                ->isInstanceOf(Match::class)
            ->object($match->getPattern())
                ->isIdenticalTo($pattern)
            ->float($match->getScore())
                ->isEqualTo(7)
        ;
    }

    public function testGetMatches()
    {
        $this
            ->given(
                $text = 'Some text.',
                $pattern1 = new \mock\HousingFinder\Domain\Model\ValueObject\Pattern('test', 'Test', ['keywordA' => 1, 'keywordB' => 1]),
                $pattern2 = new \mock\HousingFinder\Domain\Model\ValueObject\Pattern('test', 'Test', ['keyword1' => 1, 'keyword2' => 1]),
                $matcher = new \HousingFinder\Domain\Service\HousingAd\Matcher\CaseInsensitiveMaxScore($pattern1, $pattern2)
            )
            ->array($matches = $matcher->getMatches($text))
                ->hasSize(2)
            ->object($matches[0])
                ->isInstanceOf(Match::class)
            ->object($matches[1])
                ->isInstanceOf(Match::class)
        ;
    }
}
