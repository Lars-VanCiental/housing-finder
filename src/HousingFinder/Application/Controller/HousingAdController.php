<?php

namespace HousingFinder\Application\Controller;

use HousingFinder\Domain\Model\Identifier\HousingAdIdentifier;
use HousingFinder\Domain\Service\HousingAd\Importer;
use HousingFinder\Domain\Model\Entity\HousingAd;
use HousingFinder\Domain\Service\HousingAd\Source\SourceInterface;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class HousingAdController
 *
 * @package HousingFinder\Application\Controller
 */
class HousingAdController extends AbstractController implements ControllerProviderInterface
{
    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app)
    {
        $controllers = $this->getControllers();

        $controllers->get(
            '/import',
            function (Request $request) {
                return $this->view(
                    'import',
                    [
                        'error' => $request->request->get('error'),
                        'sources' => array_map(
                            function (SourceInterface $source) {
                                return $source->getName();
                            },
                            $this->application['housing_finder']['housing_add']['sources']
                        )
                    ]
                );
            }
        );

        $controllers->post(
            '/import-one',
            function (Request $request) {
                $sourceName = $request->request->get('source_name');
                $sourceUrl = $request->request->get('source_url');

                /** @var SourceInterface $source */
                $source = null;
                foreach ($this->application['housing_finder']['housing_add']['sources'] as $configuredSource) {
                    if ($configuredSource->getName() === $sourceName) {
                        $source = $configuredSource;
                    }
                }

                if (null === $source) {
                    $this->application->redirect('/ads/import?error=invalid-source');
                }

                $housingAdIdentifier = $source->getHousingAdIdentifier($sourceUrl);
                if ($this->getHousingAdRepository()->exists($housingAdIdentifier)) {
                    return $this->application->redirect('/ads/'.$housingAdIdentifier.'/show');
                }

                $housingAd = $source->getHousingAd($housingAdIdentifier);
                $housingAd->setImages($source->getHousingAdImages($housingAd));

                $this->getHousingAdRepository()->add($housingAd);
                $this->getEntityManager()->flush();

                return $this->view(
                    'browse',
                    [
                        'housingAd' => $housingAd,
                        'potentialDuplicates' => [],
                    ]
                );
            }
        );

        $controllers->get(
            '/import-auto',
            function () {
                set_time_limit(0);
                $housingAdImporter = new Importer(
                    $this->getEntityManager()->getRepository(HousingAd::class),
                    $this->application['housing_finder']['housing_add']['sources'],
                    $this->application['housing_finder']['housing_add']['matchers'],
                    $this->application['images.manager']
                );
                $importReport = $housingAdImporter->importNewHousingAd(10);
                $this->application['orm.entity_manager']->flush();

                return $this->view(
                    'import',
                    [
                        'importReport' => $importReport,
                    ]
                );
            }
        );

        $controllers->get(
            '/browse',
            function () {
                $housingAdsRepository = $this->getHousingAdRepository();
                $newHousingAds = $housingAdsRepository->findNew(1);
                $newHousingAd = null;
                $potentialDuplicates = [];
                if (!empty($newHousingAds)) {
                    $newHousingAd = reset($newHousingAds);

                    foreach ($this->getHousingAdRepository()->findDuplicateTestFields() as $row) {
                        $totalMatch = 0;
                        // Title.
                        similar_text($row['title'], $newHousingAd->getTitle(), $proximityPercentage);
                        $totalMatch += $proximityPercentage * 0.05;
                        // Description.
                        similar_text($row['description'], $newHousingAd->getDescription(), $proximityPercentage);
                        $totalMatch += $proximityPercentage * 0.75;
                        // Price.
                        similar_text($row['price'], $newHousingAd->getPrice(), $proximityPercentage);
                        $totalMatch += $proximityPercentage * 0.10;
                        // Surface.
                        similar_text($row['surface'], $newHousingAd->getSurface(), $proximityPercentage);
                        $totalMatch += $proximityPercentage * 0.10;

                        if ($totalMatch >= 65) {
                            $row['score'] = $totalMatch;
                            $potentialDuplicates[] = $row;
                        }
                    }
                }

                return $this->view(
                    'browse',
                    [
                        'housingAd' => $newHousingAd,
                        'potentialDuplicates' => $potentialDuplicates,
                    ]
                );
            }
        );

        $controllers->get(
            '/{sourceName}_{sourceIdentifier}/duplicate/{originalSourceName}_{originalSourceIdentifier}',
            function ($sourceName, $sourceIdentifier, $originalSourceName, $originalSourceIdentifier) {
                $newHousingAdIdentifier = new HousingAdIdentifier($sourceName, $sourceIdentifier);
                $newHousingAd = $this->getHousingAdRepository()->get($newHousingAdIdentifier);
                $originalHousingAdIdentifier = new HousingAdIdentifier($originalSourceName, $originalSourceIdentifier);
                $originalHousingAd = $this->getHousingAdRepository()->get($originalHousingAdIdentifier);
                $newHousingAd->setOriginal($originalHousingAd);
                $this->application['orm.entity_manager']->flush();

                return $this->application->redirect(
                    '/ads/'.$originalHousingAd->getIdentifier().'/show'
                );
            }
        );

        $controllers->get(
            '/{sourceName}_{sourceIdentifier}/pass',
            function ($sourceName, $sourceIdentifier) {
                $housingAdIdentifier = new HousingAdIdentifier($sourceName, $sourceIdentifier);
                $housingAd = $this->getHousingAdRepository()->get($housingAdIdentifier);
                $housingAd->setUpdateDate();
                $this->application['orm.entity_manager']->flush();

                return $this->application->redirect('/ads/browse');
            }
        );

        $controllers->get(
            '/list',
            function () {
                $housingAdsRepository = $this->getHousingAdRepository();

                return $this->view(
                    'list',
                    [
                        'housingAds' => $housingAdsRepository->findOnGoing([], 100),
                    ]
                );
            }
        );

        $controllers->get(
            '/{sourceName}_{sourceIdentifier}/show',
            function ($sourceName, $sourceIdentifier) {
                $housingAdIdentifier = new HousingAdIdentifier($sourceName, $sourceIdentifier);
                $housingAd = $this->getHousingAdRepository()->get($housingAdIdentifier);

                return $this->view(
                    'show',
                    [
                        'housingAd' => $housingAd,
                    ]
                );
            }
        );

        $controllers->get(
            '/{sourceName}_{sourceIdentifier}/status/{status}/to/{destination}',
            function ($sourceName, $sourceIdentifier, $status, $destination) {
                $housingAdIdentifier = new HousingAdIdentifier($sourceName, $sourceIdentifier);
                $housingAd = $this->getHousingAdRepository()->get($housingAdIdentifier);

                $housingAd->setStatus($status);
                $this->application['orm.entity_manager']->flush();

                if ($destination == 'show') {
                    $destination = $housingAd->getIdentifier().'/show';
                }

                return $this->application->redirect('/ads/'.$destination);
            }
        );

        $controllers->get(
            '/{sourceName}_{sourceIdentifier}/visit/to/{destination}',
            function ($sourceName, $sourceIdentifier, $destination, Request $request) {
                $housingAdIdentifier = new HousingAdIdentifier($sourceName, $sourceIdentifier);
                $housingAd = $this->getHousingAdRepository()->get($housingAdIdentifier);

                $visitDateGiven = $request->get('to-visit');
                $visitDate = null;
                if (!empty($visitDateGiven)) {
                    $visitDate = new \DateTime($visitDateGiven);
                }
                $housingAd->setVisitDate($visitDate);
                $this->application['orm.entity_manager']->flush();

                if ($destination == 'show') {
                    $destination = $housingAd->getIdentifier().'/show';
                }

                return $this->application->redirect('/ads/'.$destination);
            }
        );

        return $controllers;
    }

    /**
     * @return \HousingFinder\Domain\Model\HousingAdRepositoryInterface
     */
    protected function getHousingAdRepository()
    {
        return $this->getEntityManager()->getRepository(HousingAd::class);
    }
}
