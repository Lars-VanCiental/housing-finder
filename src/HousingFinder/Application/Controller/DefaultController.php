<?php

namespace HousingFinder\Application\Controller;

use HousingFinder\Domain\Model\Entity\HousingAd;
use Silex\Application;
use Silex\ControllerProviderInterface;

/**
 * Class DefaultController
 *
 * @package HousingFinder\Application\Controller
 */
class DefaultController extends AbstractController implements ControllerProviderInterface
{
    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app)
    {
        $controllers = $this->getControllers();

        $controllers->get(
            '/',
            function () use ($app) {
                return $this->view('index');
            }
        );

        return $controllers;
    }

    /**
     * @return \HousingFinder\Domain\Model\HousingAdRepositoryInterface
     */
    protected function getHousingAdRepository()
    {
        return $this->getEntityManager()->getRepository(HousingAd::class);
    }
}
