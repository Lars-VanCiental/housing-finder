<?php

namespace HousingFinder\Application\Controller;

use Silex\Application;

/**
 * Class AbstractController
 *
 * @package HousingFinder\Application\Controller
 */
class AbstractController
{
    /**
     * @var Application
     */
    protected $application;

    /**
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * @return mixed
     */
    protected function getControllers()
    {
        return $this->application['controllers_factory'];
    }

    /**
     * @param string $name
     * @param array  $parameters
     * @return mixed
     */
    protected function view(string $name, array $parameters = [])
    {
        $callingController = explode('\\', get_called_class());
        $viewDirectory = substr(end($callingController), 0, -10);

        return $this->application['twig']->render(
            $viewDirectory.'/'.$name.'.html.twig',
            $parameters
        );
    }

    /**
     * @return mixed
     */
    protected function getEntityManager()
    {
        return $this->application['orm.entity_manager'];
    }
}
