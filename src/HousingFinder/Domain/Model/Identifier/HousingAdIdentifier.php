<?php

namespace HousingFinder\Domain\Model\Identifier;

/**
 * Class HousingAdIdentifier
 *
 * @package HousingFinder\Domain\Model\ValueObject
 */
class HousingAdIdentifier
{
    /**
     * @var string
     */
    protected $sourceName;

    /**
     * @var string
     */
    protected $sourceIdentifier;

    /**
     * @var string
     */
    protected $linkToSource;

    /**
     * @param string $sourceName
     * @param string $sourceIdentifier
     * @param string $linkToSource
     */
    public function __construct(string $sourceName, string $sourceIdentifier, string $linkToSource = null)
    {
        $this->sourceName = $sourceName;
        $this->sourceIdentifier = $sourceIdentifier;
        $this->linkToSource = $linkToSource;
    }

    /**
     * @return string
     */
    public function getSourceName()
    {
        return $this->sourceName;
    }

    /**
     * @return string
     */
    public function getSourceIdentifier()
    {
        return $this->sourceIdentifier;
    }

    /**
     * @return string
     */
    public function getLinkToSource()
    {
        return $this->linkToSource;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->sourceName.'_'.$this->getSourceIdentifier();
    }
}
