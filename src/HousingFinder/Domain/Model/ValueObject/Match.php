<?php

namespace HousingFinder\Domain\Model\ValueObject;

/**
 * Class Match
 * @package HousingFinder\Domain\Model\ValueObject
 */
class Match
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var Pattern
     */
    protected $pattern;

    /**
     * @var float
     */
    protected $score;

    /**
     * @param Pattern $pattern
     * @param int     $score
     */
    public function __construct(Pattern $pattern, float $score)
    {
        $this->pattern = $pattern;
        $this->score = $score;
    }

    /**
     * @return Pattern
     */
    public function getPattern() : Pattern
    {
        return $this->pattern;
    }

    /**
     * @return float
     */
    public function getScore() : float
    {
        return $this->score;
    }
}
