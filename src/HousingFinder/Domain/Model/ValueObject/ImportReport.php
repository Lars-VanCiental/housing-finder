<?php

namespace HousingFinder\Domain\Model\ValueObject;

/**
 * Class ImportReport
 *
 * @package HousingFinder\Application\Model\ValueObject
 */
class ImportReport
{
    /**
     * @var int
     */
    protected $numberOfHousingAdSourceConsulted = 0;

    /**
     * @var int
     */
    protected $numberOfHousingAdParsed = 0;

    /**
     * @var int
     */
    protected $numberOfHousingAdAdded = 0;

    /**
     * Increase number of housing ad parsed.
     */
    public function consultHousingAdSource()
    {
        $this->numberOfHousingAdSourceConsulted++;
    }

    /**
     * @return int
     */
    public function getNumberOfHousingAdSourceConsulted() : int
    {
        return $this->numberOfHousingAdSourceConsulted;
    }

    /**
     * Increase number of housing ad parsed.
     */
    public function parseHousingAd()
    {
        $this->numberOfHousingAdParsed++;
    }

    /**
     * @return int
     */
    public function getNumberOfHousingAdParsed() : int
    {
        return $this->numberOfHousingAdParsed;
    }

    /**
     * Increase number of housing ad added.
     */
    public function addHousingAd()
    {
        $this->numberOfHousingAdAdded++;
    }

    /**
     * @return int
     */
    public function getNumberOfHousingAdAdded() : int
    {
        return $this->numberOfHousingAdAdded;
    }
}
