<?php

namespace HousingFinder\Domain\Model\ValueObject;

/**
 * Class Image
 * @package HousingFinder\Domain\Model\ValueObject
 */
class Image
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $source;

    /**
     * @var string
     */
    protected $description;

    /**
     * @param string $source
     * @param string $description
     */
    public function __construct(string $source, string $description)
    {
        $this->source = $source;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getSource() : string
    {
        return $this->source;
    }

    /**
     * @return string
     */
    public function getRelativeSource() : string
    {
        return explode('web', $this->source)[1];
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }
}
