<?php

namespace HousingFinder\Domain\Model\ValueObject;

/**
 * Class Pattern
 * @package HousingFinder\Domain\Model\ValueObject
 */
class Pattern
{
    /**
     * @var string
     */
    protected $identifier;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $matchingPatterns;

    /**
     * @param string $identifier
     * @param string $name
     * @param array  $matchingPatterns
     */
    public function __construct(string $identifier, string $name, array $matchingPatterns = [])
    {
        $this->identifier = $identifier;
        $this->name = $name;
        foreach ($matchingPatterns as $pattern => $weight) {
            $this->addMatchingPattern($pattern, $weight);
        }
    }

    /**
     * @return string
     */
    public function getIdentifier() : string
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $pattern
     * @param float  $weight
     */
    public function addMatchingPattern(string $pattern, float $weight)
    {
        $this->matchingPatterns[$pattern] = $weight;
    }

    /**
     * @return array
     */
    public function getMatchingPatterns() : array
    {
        return $this->matchingPatterns;
    }
}
