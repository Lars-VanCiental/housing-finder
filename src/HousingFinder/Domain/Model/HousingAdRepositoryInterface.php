<?php

namespace HousingFinder\Domain\Model;

use HousingFinder\Domain\Model\Entity\HousingAd;
use HousingFinder\Domain\Model\Identifier\HousingAdIdentifier;

/**
 * Class HousingAdRepositoryInterface
 *
 * @package HousingFinder\Domain\Model
 */
interface HousingAdRepositoryInterface
{
    /**
     * @param \HousingFinder\Domain\Model\Identifier\HousingAdIdentifier $housingAdIdentifier
     * @return bool
     */
    public function exists(HousingAdIdentifier $housingAdIdentifier);

    /**
     * @param HousingAd $housingAd
     */
    public function add(HousingAd $housingAd);

    /**
     * @param HousingAd $housingAd
     */
    public function remove(HousingAd $housingAd);

    /**
     * @param HousingAdIdentifier $housingAdIdentifier
     * @return HousingAd
     */
    public function get(HousingAdIdentifier $housingAdIdentifier) : HousingAd;

    /**
     * @param int $limit
     * @param int $offset
     * @return HousingAd[]
     */
    public function findNew($limit = 10, $offset = 0) : array;

    /**
     * @param array $filters
     * @param int   $limit
     * @param int   $offset
     * @return mixed
     */
    public function findOnGoing(array $filters = [], $limit = 10, $offset = 0) : array;

    /**
     * @return array
     */
    public function findDuplicateTestFields() : array;
}
