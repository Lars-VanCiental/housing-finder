<?php

namespace HousingFinder\Domain\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use HousingFinder\Domain\Model\Identifier\HousingAdIdentifier;
use HousingFinder\Domain\Model\ValueObject\Image;
use HousingFinder\Domain\Model\ValueObject\Match;

/**
 * Class HousingAd
 * @package HousingFinder\Domain\Model\Entity
 */
class HousingAd
{
    const STATUS_NEW = 'new';
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_CONTACTED = 'contacted';
    const STATUS_RESPONDED = 'responded';
    const STATUS_WAITING = 'waiting';
    const STATUS_KEPT = 'kept';
    const STATUS_REJECTED = 'rejected';
    const STATUS_DEPRECATED = 'deprecated';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var HousingAdIdentifier
     */
    protected $identifier;

    /**
     * @var \DateTime
     */
    protected $createDate;

    /**
     * @var \DateTime
     */
    protected $updateDate;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $zipCode;

    /**
     * @var float
     */
    protected $price;

    /**
     * @var float
     */
    protected $surface;

    /**
     * @var int
     */
    protected $numberOfRooms;

    /**
     * @var string
     */
    protected $ges;

    /**
     * @var string
     */
    protected $energyClass;

    /**
     * @var Image[]|ArrayCollection
     */
    protected $images;

    /**
     * @var Match[]|ArrayCollection
     */
    protected $matches;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var \DateTime
     */
    protected $visitDate;

    /**
     * @var HousingAd
     */
    protected $original;

    /**
     * @var HousingAd[]|ArrayCollection
     */
    protected $duplicates;

    /**
     * @param HousingAdIdentifier $identifier
     * @param string              $title
     * @param string              $description
     * @param string              $zipCode
     * @param float               $price
     * @param float               $surface
     * @param int                 $numberOfRooms
     * @param string              $ges
     * @param string              $energyClass
     */
    public function __construct(
        HousingAdIdentifier $identifier,
        string $title,
        string $description,
        string $zipCode,
        float $price,
        float $surface,
        int $numberOfRooms,
        string $ges,
        string $energyClass
    ) {
        $this->identifier = $identifier;
        $this->title = $title;
        $this->description = $description;
        $this->zipCode = $zipCode;
        $this->price = $price;
        $this->surface = $surface;
        $this->numberOfRooms = $numberOfRooms;
        $this->ges = $ges;
        $this->energyClass = $energyClass;

        $this->createDate = new \DateTime();
        $this->updateDate = new \DateTime();
        $this->images = new ArrayCollection();
        $this->matches = new ArrayCollection();
        $this->status = self::STATUS_NEW;
        $this->duplicates = new ArrayCollection();
    }

    /**
     * @return HousingAdIdentifier
     */
    public function getIdentifier() : HousingAdIdentifier
    {
        return $this->identifier;
    }

    /**
     * @return \DateTime
     */
    public function getCreateDate() : \DateTime
    {
        return $this->createDate;
    }

    /**
     * Housing Ad has been updated.
     */
    public function setUpdateDate()
    {
        $this->updateDate = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getUpdateDate() : \DateTime
    {
        return $this->updateDate;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getZipCode() : string
    {
        return $this->zipCode;
    }

    /**
     * @return float
     */
    public function getPrice() : float
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getSurface() : float
    {
        return $this->surface;
    }

    /**
     * @return float
     */
    public function getPricePerSurface() : float
    {
        return $this->price / $this->surface;
    }

    /**
     * @return int
     */
    public function getNumberOfRooms() : int
    {
        return $this->numberOfRooms;
    }

    /**
     * @return string
     */
    public function getGes() : string
    {
        return $this->ges;
    }

    /**
     * @return string
     */
    public function getEnergyClass() : string
    {
        return $this->energyClass;
    }

    /**
     * @param Image[] $images
     */
    public function setImages(array $images)
    {
        $this->images->clear();
        foreach ($images as $image) {
            $this->images->add($image);
        }

        $this->setUpdateDate();
    }

    /**
     * @return Image[]
     */
    public function getImages() : array
    {
        return $this->images->toArray();
    }

    /**
     * @param Match[] $matches
     */
    public function setMatches(array $matches)
    {
        $this->matches->clear();
        foreach ($matches as $match) {
            $this->matches->add($match);
        }

        $this->setUpdateDate();
    }

    /**
     * @return Match[]
     */
    public function getMatches() : array
    {
        return $this->matches->toArray();
    }

    /**
     * @param string $patternIdentifier
     * @return Match[]
     */
    public function getMatchByPatternIdentifier($patternIdentifier) : array
    {
        return $this->matches->filter(
            function (Match $match) use ($patternIdentifier) {
                return $match->getPattern()->getIdentifier() == $patternIdentifier;
            }
        );
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        if (!in_array($status, $this->getNextStatus())) {
            throw new \InvalidArgumentException('Invalid status given. See HousingAd::STATUS_* others than NEW.');
        }

        $this->status = $status;

        $this->setUpdateDate();
    }

    /**
     * @return string
     */
    public function getStatus() : string
    {
        return $this->status;
    }

    /**
     * @return string[]
     */
    public function getNextStatus() : array
    {
        switch ($this->status) {
            case self::STATUS_NEW:
                return [
                    self::STATUS_ACCEPTED,
                    self::STATUS_CONTACTED,
                    self::STATUS_RESPONDED,
                    self::STATUS_WAITING,
                    self::STATUS_KEPT,
                    self::STATUS_REJECTED,
                    self::STATUS_DEPRECATED,
                ];
            case self::STATUS_ACCEPTED:
                return [
                    self::STATUS_CONTACTED,
                    self::STATUS_RESPONDED,
                    self::STATUS_WAITING,
                    self::STATUS_KEPT,
                    self::STATUS_REJECTED,
                    self::STATUS_DEPRECATED,
                ];
            case self::STATUS_CONTACTED:
                return [
                    self::STATUS_RESPONDED,
                    self::STATUS_WAITING,
                    self::STATUS_KEPT,
                    self::STATUS_REJECTED,
                    self::STATUS_DEPRECATED,
                ];
            case self::STATUS_RESPONDED:
                return [
                    self::STATUS_CONTACTED,
                    self::STATUS_WAITING,
                    self::STATUS_KEPT,
                    self::STATUS_REJECTED,
                    self::STATUS_DEPRECATED,
                ];
            case self::STATUS_WAITING:
                return [
                    self::STATUS_CONTACTED,
                    self::STATUS_RESPONDED,
                    self::STATUS_KEPT,
                    self::STATUS_REJECTED,
                    self::STATUS_DEPRECATED,
                ];
            case self::STATUS_KEPT:
                return [
                    self::STATUS_CONTACTED,
                    self::STATUS_RESPONDED,
                    self::STATUS_WAITING,
                    self::STATUS_REJECTED,
                    self::STATUS_DEPRECATED,
                ];
            case self::STATUS_REJECTED:
                return [
                    self::STATUS_ACCEPTED,
                    self::STATUS_CONTACTED,
                    self::STATUS_RESPONDED,
                    self::STATUS_WAITING,
                    self::STATUS_KEPT,
                    self::STATUS_DEPRECATED,
                ];
            case self::STATUS_DEPRECATED:
                return [
                    self::STATUS_CONTACTED,
                    self::STATUS_RESPONDED,
                    self::STATUS_WAITING,
                    self::STATUS_KEPT,
                    self::STATUS_REJECTED,
                ];
        }

        throw new \RuntimeException('Invalid status was set.');
    }

    /**
     * @param \DateTime|null $visitDate
     */
    public function setVisitDate(\DateTime $visitDate = null)
    {
        $this->visitDate = $visitDate;
    }

    /**
     * @return \DateTime
     */
    public function getVisitDate() : \DateTime
    {
        return $this->visitDate;
    }

    /**
     * @return bool
     */
    public function isVisitScheduled() : bool
    {
        return $this->visitDate != null;
    }

    /**
     * @return bool
     */
    public function isVisitDone() : bool
    {
        return $this->isVisitScheduled() && ($this->visitDate < (new \DateTime()));
    }

    /**
     * @param HousingAd $originalHousingAd
     */
    public function setOriginal(HousingAd $originalHousingAd = null)
    {
        if (null !== $this->original) {
            $this->original->removeDuplicate($this);
        }

        $this->original = $originalHousingAd;

        if (null !== $this->original) {
            $originalHousingAd->addDuplicate($this);
        }

        $this->setUpdateDate();
    }

    /**
     * @return HousingAd
     */
    public function getOriginal()
    {
        return $this->original;
    }

    /**
     * @param HousingAd $duplicateHousingAd
     */
    public function addDuplicate(HousingAd $duplicateHousingAd)
    {
        if ($this !== $duplicateHousingAd->getOriginal()) {
            throw new \InvalidArgumentException('Use setOriginal to set a duplicate housing ad.');
        }

        $this->duplicates->add($duplicateHousingAd);

        $this->setUpdateDate();
    }

    /**
     * @param HousingAd $duplicateHousingAd
     */
    public function removeDuplicate(HousingAd $duplicateHousingAd)
    {
        if ($this !== $duplicateHousingAd->getOriginal()) {
            throw new \InvalidArgumentException('Use setOriginal to remove a duplicate housing ad.');
        }

        $this->duplicates->removeElement($duplicateHousingAd);

        $this->setUpdateDate();
    }

    /**
     * @return HousingAd[]|ArrayCollection
     */
    public function getDuplicates()
    {
        return $this->duplicates;
    }
}
