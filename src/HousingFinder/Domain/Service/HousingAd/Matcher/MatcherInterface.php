<?php

namespace HousingFinder\Domain\Service\HousingAd\Matcher;

use HousingFinder\Domain\Model\ValueObject\Match;

/**
 * Interface MatcherInterface
 * @package HousingFinder\Domain\Service\HousingAd
 */
interface MatcherInterface
{
    /**
     * @param string $text
     * @return Match[]
     */
    public function getMatches(string $text) : array;
}
