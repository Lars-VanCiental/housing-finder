<?php

namespace HousingFinder\Domain\Service\HousingAd\Matcher;

use HousingFinder\Domain\Model\ValueObject\Match;
use HousingFinder\Domain\Model\ValueObject\Pattern;
use HousingFinder\Domain\Service\HousingAd\Matcher\MatcherInterface;

/**
 * Class CaseInsensitive
 * @package HousingFinder\Domain\Service\HousingAd\Matcher
 */
class CaseInsensitiveMaxScore implements MatcherInterface
{
    /**
     * @var Pattern[]
     */
    protected $patterns;

    /**
     * @param Pattern ...$patterns
     */
    public function __construct(Pattern ...$patterns)
    {
        $this->patterns = $patterns;
    }

    /**
     * @param string $text
     * @return Match[]
     */
    public function getMatches(string $text) : array
    {
        $matches = [];

        foreach ($this->patterns as $matcher) {
            $matches[] = static::getMatch($text, $matcher);
        }

        return $matches;
    }

    /**
     * @param string  $text
     * @param Pattern $pattern
     * @return Match
     */
    public static function getMatch(string $text, Pattern $pattern) : Match
    {
        $score = 0;

        $text = strtoupper($text);
        foreach ($pattern->getMatchingPatterns() as $keyword => $weight) {
            $score += substr_count($text, strtoupper($keyword)) * $weight;
        }

        return new Match($pattern, $score);
    }
}
