<?php

namespace HousingFinder\Domain\Service\HousingAd;

use HousingFinder\Domain\Model\Entity\HousingAd;
use HousingFinder\Domain\Model\Identifier\HousingAdIdentifier;
use HousingFinder\Domain\Model\ValueObject\Image;

/**
 * Class ImageManager
 *
 * @package HousingFinder\Domain\Service\HousingAd
 */
class ImageManager
{
    protected $baseDirectoryPath;

    /**
     * @param $baseDirectoryPath
     */
    public function __construct($baseDirectoryPath)
    {
        $this->baseDirectoryPath = $baseDirectoryPath;
    }

    /**
     * @param HousingAdIdentifier $housingAdIdentifier
     * @return string
     */
    public function getHousingAdDirectoryPath(HousingAdIdentifier $housingAdIdentifier)
    {
        return self::buildPath(
            $this->baseDirectoryPath,
            $housingAdIdentifier->getSourceName(),
            $housingAdIdentifier->getSourceIdentifier()
        );
    }

    /**
     * @param HousingAdIdentifier $housingAdIdentifier
     * @return string
     */
    public function createHousingAdDirectory(HousingAdIdentifier $housingAdIdentifier)
    {
        $housingAdDirectory = $this->getHousingAdDirectoryPath($housingAdIdentifier);

        self::assureDirectoryExists($housingAdDirectory);

        return$housingAdDirectory;
    }

    /**
     * @param HousingAd $housingAd
     * @param string    $imageSource
     * @param string    $description
     * @return Image
     */
    public function createHousingAdImage(
        HousingAd $housingAd,
        string $imageSource,
        string $description
    ) {
        $housingAdDirectoryPath = $this->createHousingAdDirectory($housingAd->getIdentifier());

        $imagePath = self::buildPath($housingAdDirectoryPath, uniqid('image_').'.jpg');
        if (!@copy($imageSource, $imagePath)) {
            throw new \InvalidArgumentException(
                'Could not copy image from "'.$imageSource.'".'
            );
        }

        return new Image($imagePath, $description);
    }

    /**
     * @param string ...$pathParts
     * @return string
     */
    protected static function buildPath(string ...$pathParts)
    {
        // Remove trailing directory separator.
        array_walk(
            $pathParts,
            function ($pathPart) {
                return rtrim(
                    preg_replace('#[a-zA-Z_-]#', '-', $pathPart),
                    '/'
                );
            }
        );

        // Implode not null parts.
        return implode(
            '/',
            array_filter($pathParts)
        );
    }

    /**
     * @param string $directoryPath
     */
    protected static function assureDirectoryExists(string $directoryPath)
    {
        if (!is_dir($directoryPath)) {
            if (!mkdir($directoryPath, null, true)) {
                throw new \RuntimeException('Could not create directory : "'.$directoryPath.'"".');
            }
        }
    }

    /**
     * @param string $directoryPath
     */
    protected static function tryRemoveDirectory(string $directoryPath)
    {
        if (is_dir($directoryPath)) {
            $directory = new \FilesystemIterator($directoryPath, \FilesystemIterator::SKIP_DOTS);
            if (0 == iterator_count($directory)) {
                if (rmdir($directoryPath)) {
                    self::tryRemoveDirectory(dirname($directoryPath));

                    return true;
                }
            }
        }

        return false;
    }
}
