<?php

namespace HousingFinder\Domain\Service\HousingAd\Source;

use HousingFinder\Domain\Model\Entity\HousingAd;
use HousingFinder\Domain\Model\Identifier\HousingAdIdentifier;
use HousingFinder\Domain\Model\ValueObject\Image;

/**
 * Class LeBonCoin
 * @package HousingFinder\Domain\Service\HousingAd\Source
 */
class LeBonCoin extends AbstractHtmlTwoStepCrawler implements SourceInterface
{

    /**
     * @param string $sourceUrl
     * @return HousingAdIdentifier
     */
    public function getHousingAdIdentifier(string $sourceUrl) : HousingAdIdentifier
    {
        preg_match('#\/(?<identifier>\d+)\.htm#', $sourceUrl, $sourceIdentifier);

        return new HousingAdIdentifier(
            $this->name,
            $sourceIdentifier['identifier'],
            $sourceUrl
        );
    }

    /**
     * @param \simple_html_dom $housingAdsDom
     * @return HousingAdIdentifier[]
     */
    protected function getHousingAdIdentifiers(\simple_html_dom $housingAdsDom) : array
    {
        $housingAdIdentifiers = [];

        $housingAdsLinksDom = $housingAdsDom->find('div.list-lbc a[title]');
        foreach ($housingAdsLinksDom as $housingAdLinkDom) {
            if ($housingAdLinkDom->class != 'alertsLink') {
                $housingAdIdentifiers[] = new HousingAdIdentifier(
                    $this->name,
                    $this->getSourceIdentifier($housingAdLinkDom),
                    'http:'.$housingAdLinkDom->href
                );
            }
        }

        return $housingAdIdentifiers;
    }

    /**
     * @param \simple_html_dom_node $housingAdLinkDom
     * @return string
     */
    protected function getSourceIdentifier(\simple_html_dom_node $housingAdLinkDom) : string
    {
        preg_match(
            '#.*\/(?<id>\d+)\.htm\?ca=22_s$#',
            $housingAdLinkDom->href,
            $matches
        );

        if (empty($matches['id'])) {
            throw new \InvalidArgumentException('Could not find housing ad identifier.');
        }

        return $matches['id'];
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @return string
     */
    protected function getTitle(\simple_html_dom $housingAdDom) : string
    {
        return utf8_encode((string) $housingAdDom->find('h1')[0]->plaintext);
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @return string
     */
    protected function getDescription(\simple_html_dom $housingAdDom) : string
    {
        return utf8_encode((string) $housingAdDom->find('div.content[itemprop=description]')[0]->plaintext);
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @return string
     */
    protected function getZipCode(\simple_html_dom $housingAdDom) : string
    {
        return utf8_encode((string) $housingAdDom->find('[itemprop=postalCode]')[0]->plaintext);
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @return float
     */
    protected function getPrice(\simple_html_dom $housingAdDom) : float
    {
        return (float) utf8_encode(
            preg_replace(
                '#[^\d]#',
                '',
                (string) $housingAdDom->find('span.price[itemprop=price]')[0]->plaintext
            )
        );
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @return float
     */
    protected function getSurface(\simple_html_dom $housingAdDom) : float
    {
        $surface = '';
        foreach ($housingAdDom->find('div.criterias table th') as $criterionIndex => $criterionNameDom) {
            if (preg_match('#surface :#i', (string) $criterionNameDom)) {
                $surface = preg_replace(
                    '#(<sup>2</sup>)|[^\d]#',
                    '',
                    (string) $housingAdDom->find('div.criterias table td')[$criterionIndex]
                );
                break;
            }
        }

        return (float) utf8_encode($surface);
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @return int
     */
    protected function getNumberOfRooms(\simple_html_dom $housingAdDom) : int
    {
        $numberOfRooms = '';
        foreach ($housingAdDom->find('div.criterias table th') as $criterionIndex => $criterionNameDom) {
            if (preg_match('#pi.?ces :#i', (string) $criterionNameDom)) {
                $numberOfRooms = (string) $housingAdDom->find('div.criterias table td')[$criterionIndex]->plaintext;
                break;
            }
        }

        return $numberOfRooms;
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @return string
     */
    protected function getGES(\simple_html_dom $housingAdDom) : string
    {
        $ges = '';
        foreach ($housingAdDom->find('div.criterias table th') as $criterionIndex => $criterionNameDom) {
            if (preg_match('#ges :#i', (string) $criterionNameDom)) {
                preg_match(
                    '# target="_blank">(?<ges>[A-Z])#',
                    (string) $housingAdDom->find('div.criterias table td')[$criterionIndex],
                    $matches
                );
                $ges = $matches['ges'];
                break;
            }
        }

        return utf8_encode($ges);
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @return string
     */
    protected function getDPE(\simple_html_dom $housingAdDom) : string
    {
        $dpe = '';
        foreach ($housingAdDom->find('div.criterias table th') as $criterionIndex => $criterionNameDom) {
            if (preg_match('#classe .?nergie :#i', (string) $criterionNameDom)) {
                preg_match(
                    '# target="_blank">(?<dpe>[A-Z])#',
                    (string) $housingAdDom->find('div.criterias table td')[$criterionIndex],
                    $matches
                );
                $dpe = $matches['dpe'];
                break;
            }
        }

        return utf8_encode($dpe);
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @param HousingAd        $housingAd
     * @return Image[]
     */
    protected function getImages(\simple_html_dom $housingAdDom, HousingAd $housingAd) : array
    {
        $images = [];

        foreach ($housingAdDom->find('div.thumbs_carousel_window a.thumbs_cadre span.thumbs') as $imageSpanDom) {
            try {
                $images[] = $this->imageManager->createHousingAdImage(
                    $housingAd,
                    str_replace(
                        ['background-image: url(\'//', '\');', 'thumbs'],
                        ['http://', '', 'images'],
                        $imageSpanDom->style
                    ),
                    ''
                );
            } catch (\InvalidArgumentException $e) {
                // Fail to copy image.
            }
        }

        return $images;
    }
}
