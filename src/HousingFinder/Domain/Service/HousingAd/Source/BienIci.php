<?php

namespace HousingFinder\Domain\Service\HousingAd\Source;

use HousingFinder\Domain\Model\Entity\HousingAd;
use HousingFinder\Domain\Model\Identifier\HousingAdIdentifier;
use HousingFinder\Domain\Model\ValueObject\Image;
use HousingFinder\Domain\Service\HousingAd\ImageManager;
use HousingFinder\Domain\Service\Utility\RandomQueryContextTrait;

/**
 * Class BienIci
 * @package HousingFinder\Domain\Service\HousingAd\Source
 */
class BienIci implements SourceInterface
{
    use RandomQueryContextTrait;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $query;

    /**
     * @var array
     */
    protected $filters;

    /**
     * @var ImageManager
     */
    protected $imageManager;

    /**
     * @param string       $name
     * @param string       $query
     * @param array        $filters
     * @param ImageManager $imageManager
     */
    public function __construct(string $name, string $query, array $filters, ImageManager $imageManager)
    {
        $this->name = $name;
        $this->query = $query;
        $this->filters = $filters;
        $this->imageManager = $imageManager;
    }

    /**
     * @return string|string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $sourceUrl
     * @return HousingAdIdentifier
     */
    public function getHousingAdIdentifier(string $sourceUrl) : HousingAdIdentifier
    {
        preg_match('#\/(?<identifier>\w+)\?#', $sourceUrl, $sourceIdentifier);

        return new HousingAdIdentifier(
            $this->name,
            $sourceIdentifier['identifier'],
            $sourceUrl
        );
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return HousingAd[]
     */
    public function getHousingAds(int $limit, int $offset = 0) : array
    {
        $housingAds = [];

        $apiResponse = file_get_contents(
            $this->getQueryUrl($limit, $offset),
            false,
            $this->getQueryContext()
        );
        $responseContent = json_decode($apiResponse, JSON_OBJECT_AS_ARRAY);
        if ($responseContent) {
            foreach ($responseContent['realEstateAds'] as $realEstateAd) {
                $housingAds[] = $this->parseHousingAd($realEstateAd);
            }
        }

        return $housingAds;
    }

    /**
     * @param HousingAdIdentifier $housingAdIdentifier
     * @return HousingAd
     */
    public function getHousingAd(HousingAdIdentifier $housingAdIdentifier) : HousingAd
    {
        $apiResponse = file_get_contents(
            $this->getHousingAdDataUrl($housingAdIdentifier),
            false,
            $this->getQueryContext()
        );
        $responseContent = json_decode($apiResponse, JSON_OBJECT_AS_ARRAY);

        return $this->parseHousingAd($responseContent);
    }

    /**
     * @param HousingAd $housingAd
     * @return Image[]
     */
    public function getHousingAdImages(HousingAd $housingAd) : array
    {
        $apiResponse = @file_get_contents(
            $this->getHousingAdDataUrl($housingAd->getIdentifier()),
            false,
            $this->getQueryContext()
        );

        if ($apiResponse && ($responseContent = json_decode($apiResponse, JSON_OBJECT_AS_ARRAY))) {
            return $this->getImages($responseContent, $housingAd);
        }

        return [];
    }

    /**
     * @param $limit
     * @param $offset
     * @return string
     */
    protected function getQueryUrl($limit, $offset)
    {
        $filters = array_merge(
            $this->filters,
            [
                'size' => $limit,
                'from' => $offset,
                'page' => 1,
            ]
        );

        return $this->query.urlencode(json_encode($filters));
    }

    /**
     * @param HousingAdIdentifier $housingAdIdentifier
     * @return string
     */
    protected function getHousingAdDataUrl(HousingAdIdentifier $housingAdIdentifier) : string
    {
        return 'https://www.bienici.com/realEstateAd.json?id='.$housingAdIdentifier->getSourceIdentifier();
    }

    /**
     * @param array $realEstateAd
     * @return HousingAd
     */
    protected function parseHousingAd(array $realEstateAd) : HousingAd
    {
        $housingAdIdentifier = new HousingAdIdentifier(
            $this->name,
            $realEstateAd['id'],
            'http://www.bienici.com/annonce/'.$realEstateAd['id']
        );

        return new HousingAd(
            $housingAdIdentifier,
            !empty($realEstateAd['title']) ? html_entity_decode($realEstateAd['title']) : '',
            html_entity_decode($realEstateAd['description']),
            $realEstateAd['postalCode'],
            $realEstateAd['price'],
            $realEstateAd['surfaceArea'],
            $realEstateAd['roomsQuantity'] ?? 0,
            !empty($realEstateAd['energyClassification']) ? $realEstateAd['energyClassification'] : '',
            !empty($realEstateAd['greenhouseGazClassification']) ? $realEstateAd['greenhouseGazClassification'] : ''
        );
    }

    /**
     * @param array     $realEstateAd
     * @param HousingAd $housingAd
     * @return Image[]
     */
    protected function getImages(array $realEstateAd, HousingAd $housingAd) : array
    {
        $images = [];

        foreach ($realEstateAd['photos'] as $photo) {
            try {
                $images[] = $this->imageManager->createHousingAdImage(
                    $housingAd,
                    $photo['url_photo'] ?? 'http:'.$photo['url'],
                    ''
                );
            } catch (\InvalidArgumentException $e) {
                // Fail to copy image.
            }
        }

        return $images;
    }
}
