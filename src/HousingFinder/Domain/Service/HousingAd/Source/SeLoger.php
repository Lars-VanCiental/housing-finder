<?php

namespace HousingFinder\Domain\Service\HousingAd\Source;

use HousingFinder\Domain\Model\Entity\HousingAd;
use HousingFinder\Domain\Model\Identifier\HousingAdIdentifier;
use HousingFinder\Domain\Model\ValueObject\Image;

/**
 * Class SeLoger
 * @package HousingFinder\Domain\Service\HousingAd\Source
 */
class SeLoger extends AbstractHtmlTwoStepCrawler implements SourceInterface
{

    /**
     * @param string $sourceUrl
     * @return HousingAdIdentifier
     */
    public function getHousingAdIdentifier(string $sourceUrl) : HousingAdIdentifier
    {
        preg_match('#\/(?<identifier>\d+)\.htm#', $sourceUrl, $sourceIdentifier);

        return new HousingAdIdentifier(
            $this->name,
            $sourceIdentifier['identifier'],
            $sourceUrl
        );
    }

    /**
     * @param \simple_html_dom $housingAdsDom
     * @return HousingAdIdentifier[]
     */
    protected function getHousingAdIdentifiers(\simple_html_dom $housingAdsDom) : array
    {
        $housingAdIdentifiers = [];

        $housingAdsLinksDom = $housingAdsDom->find('a.listing_link');
        foreach ($housingAdsLinksDom as $housingAdLinkDom) {
            $housingAdIdentifiers[] = new HousingAdIdentifier(
                $this->name,
                $this->getSourceIdentifier($housingAdLinkDom),
                $housingAdLinkDom->href
            );
        }

        return $housingAdIdentifiers;
    }

    /**
     * @param \simple_html_dom_node $housingAdLinkDom
     * @return string
     */
    protected function getSourceIdentifier(\simple_html_dom_node $housingAdLinkDom) : string
    {
        preg_match(
            '#.*\/(?<id>\d+)\.htm\?.*$#',
            $housingAdLinkDom->href,
            $matches
        );

        if (empty($matches['id'])) {
            throw new \InvalidArgumentException('Could not find housing ad identifier.');
        }

        return $matches['id'];
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @return string
     */
    protected function getTitle(\simple_html_dom $housingAdDom) : string
    {
        return (string) preg_replace(
            ['# +#', '#\r\n#', '#<span[^\/]+</span>#'],
            [' ', '', ''],
            $housingAdDom->find('h1')[0]->plaintext
        );
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @return string
     */
    protected function getDescription(\simple_html_dom $housingAdDom) : string
    {
        return (string) $housingAdDom->find('input[name=description]')[0]->value;
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @return string
     */
    protected function getZipCode(\simple_html_dom $housingAdDom) : string
    {
        return (string) $housingAdDom->find('input[name=codepostal]')[0]->value;
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @return float
     */
    protected function getPrice(\simple_html_dom $housingAdDom) : float
    {
        return (float) preg_replace(
            '#[^\d]#',
            '',
            (string) $housingAdDom->find('span.resume__prix')[0]->plaintext
        );
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @return float
     */
    protected function getSurface(\simple_html_dom $housingAdDom) : float
    {
        $surface = (float) preg_replace(
            '#[^\d]#',
            '',
            $housingAdDom->find('li.liste__item[title^=Surface de ]')[0]->plaintext
        );

        return $surface;
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @return int
     */
    protected function getNumberOfRooms(\simple_html_dom $housingAdDom) : int
    {
        $numberOfRoomsIndex = 0;
        if (count($housingAdDom->find('ol.resume__liste_critere li.critere_exclusive')) > 0) {
            $numberOfRoomsIndex++;
        }

        $numberOfRooms = preg_replace(
            '#[^\d]#',
            '',
            $housingAdDom->find('li.resume__critere')[$numberOfRoomsIndex]->plaintext
        );

        return $numberOfRooms;
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @return string
     */
    protected function getGES(\simple_html_dom $housingAdDom) : string
    {
        $ges = '';
        foreach ($housingAdDom->find('li.item-dpeges a') as $criterionIndex => $criterionNameDom) {
            if (preg_match('#ges : (?<ges>[A-Z])#i', (string) $criterionNameDom, $matches)) {
                $ges = $matches['ges'];
                break;
            }
        }

        return $ges;
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @return string
     */
    protected function getDPE(\simple_html_dom $housingAdDom) : string
    {
        $dpe = '';
        foreach ($housingAdDom->find('li.item-dpeges a') as $criterionIndex => $criterionNameDom) {
            if (preg_match('#dpe : (?<dpe>[A-Z])#i', (string) $criterionNameDom, $matches)) {
                $dpe = $matches['dpe'];
                break;
            }
        }

        return $dpe;
    }

    /**
     * @param \simple_html_dom $housingAdDom
     * @param HousingAd        $housingAd
     * @return Image[]
     */
    protected function getImages(\simple_html_dom $housingAdDom, HousingAd $housingAd) : array
    {
        $images = [];

        foreach ($housingAdDom->find('img.carrousel_image_visu') as $imageImgDom) {
            try {
                $images[] = $this->imageManager->createHousingAdImage(
                    $housingAd,
                    str_replace('c175', 'bigs', $imageImgDom->src),
                    $imageImgDom->alt
                );
            } catch (\InvalidArgumentException $e) {
                // Fail to copy image.
            }
        }

        return $images;
    }
}
