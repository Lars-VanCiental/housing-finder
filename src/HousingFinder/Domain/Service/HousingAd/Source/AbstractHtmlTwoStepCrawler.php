<?php

namespace HousingFinder\Domain\Service\HousingAd\Source;

use HousingFinder\Domain\Model\Entity\HousingAd;
use HousingFinder\Domain\Model\Identifier\HousingAdIdentifier;
use HousingFinder\Domain\Model\ValueObject\Image;
use HousingFinder\Domain\Service\HousingAd\ImageManager;
use HousingFinder\Domain\Service\Utility\RandomQueryContextTrait;
use Sunra\PhpSimple\HtmlDomParser;

/**
 * Class AbstractHtmlTwoTimeCrawler
 * @package HousingFinder\Domain\Service\HousingAd\Source
 */
abstract class AbstractHtmlTwoStepCrawler
{
    use RandomQueryContextTrait;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $query;

    /**
     * @var string
     */
    protected $pagination;

    /**
     * @var ImageManager
     */
    protected $imageManager;

    /**
     * @param string       $name
     * @param string       $query
     * @param string       $pagination
     * @param ImageManager $imageManager
     */
    public function __construct(string $name, string $query, string $pagination, ImageManager $imageManager)
    {
        $this->name = $name;
        $this->query = $query;
        $this->pagination = $pagination;
        $this->imageManager = $imageManager;
    }

    /**
     * @return string|string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return HousingAd[]
     */
    public function getHousingAds(int $limit, int $offset = 0) : array
    {
        $housingAds = [];
        $page = 1;

        do {
            $housingAdsDom = HtmlDomParser::file_get_html(
                $this->getQueryUrl($page),
                false,
                $this->getQueryContext()
            );

            $numberOfResultsFound = 0;
            // We don't want more results if none is found.
            foreach ($this->getHousingAdIdentifiers($housingAdsDom) as $housingAdIdentifier) {
                $numberOfResultsFound++;
                if ($offset > $numberOfResultsFound) {
                    continue;
                }

                // Wait 5-15 seconds between calls.
                usleep(rand(5000000, 15000000));
                $housingAds[] = $this->getHousingAd($housingAdIdentifier);

                if ($limit <= count($housingAds)) {
                    break 2;
                }
            }

            if (($numberOfResultsFound > 0) && ($limit > count($housingAds))) {
                $page++;
                continue;
            }
        } while ($numberOfResultsFound > 0);

        return $housingAds;
    }

    /**
     * @param HousingAdIdentifier $housingAdIdentifier
     * @return HousingAd
     */
    public function getHousingAd(HousingAdIdentifier $housingAdIdentifier) : HousingAd
    {
        $housingAdDom = HtmlDomParser::file_get_html(
            $housingAdIdentifier->getLinkToSource(),
            false,
            $this->getQueryContext()
        );

        return $this->parseHousingAd($housingAdIdentifier, $housingAdDom);
    }

    /**
     * @param HousingAdIdentifier $housingAdIdentifier
     * @param \simple_html_dom    $housingAdDom
     * @return HousingAd
     */
    public function parseHousingAd(HousingAdIdentifier $housingAdIdentifier, \simple_html_dom $housingAdDom) : HousingAd
    {
        return new HousingAd(
            $housingAdIdentifier,
            $this->getTitle($housingAdDom),
            $this->getDescription($housingAdDom),
            $this->getZipCode($housingAdDom),
            (float) $this->getPrice($housingAdDom),
            $this->getSurface($housingAdDom),
            $this->getNumberOfRooms($housingAdDom),
            $this->getGES($housingAdDom),
            $this->getDPE($housingAdDom)
        );
    }

    /**
     * @param HousingAd $housingAd
     * @return Image[]
     */
    public function getHousingAdImages(HousingAd $housingAd) : array
    {
        // Wait 5-15 seconds between calls.
        usleep(rand(5000000, 15000000));
        $housingAdDom = @HtmlDomParser::file_get_html(
            $housingAd->getIdentifier()->getLinkToSource(),
            false,
            $this->getQueryContext()
        );

        if (!$housingAdDom) {
            return [];
        }

        return $this->getImages($housingAdDom, $housingAd);
    }

    /**
     * @param int|string $page
     * @return string
     */
    protected function getQueryUrl($page)
    {
        if (is_int($page) && $page > 1 && $this->pagination != null) {
            return $this->query.$this->pagination.$page;
        }

        return $this->query;
    }

    /**
     * @param \simple_html_dom $housingAdsDom
     * @return HousingAdIdentifier[]
     */
    abstract protected function getHousingAdIdentifiers(\simple_html_dom $housingAdsDom) : array;

    /**
     * @param \simple_html_dom_node $housingAdLinkDom
     * @return string
     */
    abstract protected function getSourceIdentifier(\simple_html_dom_node $housingAdLinkDom) : string;

    /**
     * @param \simple_html_dom $housingAdDom
     * @return string
     */
    abstract protected function getTitle(\simple_html_dom $housingAdDom) : string;

    /**
     * @param \simple_html_dom $housingAdDom
     * @return string
     */
    abstract protected function getDescription(\simple_html_dom $housingAdDom) : string;

    /**
     * @param \simple_html_dom $housingAdDom
     * @return string
     */
    abstract protected function getZipCode(\simple_html_dom $housingAdDom) : string;

    /**
     * @param \simple_html_dom $housingAdDom
     * @return float
     */
    abstract protected function getPrice(\simple_html_dom $housingAdDom) : float;

    /**
     * @param \simple_html_dom $housingAdDom
     * @return float
     */
    abstract protected function getSurface(\simple_html_dom $housingAdDom) : float;

    /**
     * @param \simple_html_dom $housingAdDom
     * @return int
     */
    abstract protected function getNumberOfRooms(\simple_html_dom $housingAdDom) : int;

    /**
     * @param \simple_html_dom $housingAdDom
     * @return string
     */
    abstract protected function getGES(\simple_html_dom $housingAdDom) : string;

    /**
     * @param \simple_html_dom $housingAdDom
     * @return string
     */
    abstract protected function getDPE(\simple_html_dom $housingAdDom) : string;

    /**
     * @param \simple_html_dom $housingAdDom
     * @param HousingAd        $housingAd
     * @return Image[]
     */
    abstract protected function getImages(\simple_html_dom $housingAdDom, HousingAd $housingAd) : array;
}
