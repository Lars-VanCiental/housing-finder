<?php

namespace HousingFinder\Domain\Service\HousingAd\Source;

use HousingFinder\Domain\Model\Entity\HousingAd;
use HousingFinder\Domain\Model\Identifier\HousingAdIdentifier;
use HousingFinder\Domain\Model\ValueObject\Image;

/**
 * Interface SourceInterface
 * @package HousingFinder\Domain\Service\HousingAd
 */
interface SourceInterface
{
    /**
     * @return string
     */
    public function getName() : string;

    /**
     * @param string $sourceUrl
     * @return HousingAdIdentifier
     */
    public function getHousingAdIdentifier(string $sourceUrl) : HousingAdIdentifier;

    /**
     * @param int $limit
     * @param int $offset
     * @return HousingAd[]
     */
    public function getHousingAds(int $limit, int $offset = 0) : array;

    /**
     * @param HousingAdIdentifier $housingAdIdentifier
     * @return HousingAd
     */
    public function getHousingAd(HousingAdIdentifier $housingAdIdentifier) : HousingAd;

    /**
     * @param HousingAd $housingAd
     * @return Image[]
     */
    public function getHousingAdImages(HousingAd $housingAd) : array;
}
