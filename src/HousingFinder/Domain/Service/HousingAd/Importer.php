<?php

namespace HousingFinder\Domain\Service\HousingAd;

use HousingFinder\Domain\Model\Entity\HousingAd;
use HousingFinder\Domain\Model\ValueObject\ImportReport;
use HousingFinder\Domain\Model\HousingAdRepositoryInterface;
use HousingFinder\Domain\Service\HousingAd\Matcher\MatcherInterface;
use HousingFinder\Domain\Service\HousingAd\Source\SourceInterface;

/**
 * Class HousingAdImporter
 *
 * @package HousingFinder\Application\Service
 */
class Importer
{
    /**
     * @var HousingAdRepositoryInterface
     */
    protected $housingAdRepository;

    /**
     * @var SourceInterface[]
     */
    protected $housingAdSources;

    /**
     * @var MatcherInterface[]
     */
    protected $housingAdMatchers;

    /**
     * @var ImageManager
     */
    protected $imageManager;

    /**
     * @param HousingAdRepositoryInterface $housingAdRepository
     * @param array                        $housingAdSources
     * @param array                        $housingAdMatchers
     * @param ImageManager                 $imageManager
     */
    public function __construct(
        HousingAdRepositoryInterface $housingAdRepository,
        array $housingAdSources,
        array $housingAdMatchers,
        ImageManager $imageManager
    ) {
        $this->housingAdRepository = $housingAdRepository;

        foreach ($housingAdSources as $housingAdSource) {
            if ($housingAdSource instanceof SourceInterface) {
                $this->housingAdSources[$housingAdSource->getName()] = $housingAdSource;
            }
        }

        foreach ($housingAdMatchers as $housingAdMatcher) {
            if ($housingAdMatcher instanceof MatcherInterface) {
                $this->housingAdMatchers[] = $housingAdMatcher;
            }
        }

        $this->imageManager = $imageManager;
    }

    /**
     * @param int $limitBySource
     * @param int $offsetBySource
     * @return ImportReport
     */
    public function importNewHousingAd(int $limitBySource, int $offsetBySource = 0) : ImportReport
    {
        $importReport = new ImportReport();

        foreach ($this->housingAdSources as $housingAdSource) {
            $importReport->consultHousingAdSource();

            foreach ($housingAdSource->getHousingAds($limitBySource, $offsetBySource) as $housingAd) {
                $importReport->parseHousingAd();

                if ($this->addHousingAd($housingAd)) {
                    $importReport->addHousingAd();
                }
            }
        }

        return $importReport;
    }

    /**
     * @param HousingAd $housingAd
     * @return bool
     */
    protected function addHousingAd(HousingAd $housingAd) : bool
    {
        // Check if housing ad already exists.
        if (!$this->housingAdRepository->exists($housingAd->getIdentifier())) {
            // Import images.
            $housingAd->setImages(
                $this->housingAdSources[$housingAd->getIdentifier()->getSourceName()]->getHousingAdImages($housingAd)
            );

            // Create matches.
            foreach ($this->housingAdMatchers as $housingMatcher) {
                $housingAd->setMatches(
                    $housingMatcher->getMatches(
                        $housingAd->getTitle()."\n".$housingAd->getDescription()
                    )
                );
            }

            // Add to the repository.
            $this->housingAdRepository->add($housingAd);

            return true;
        }

        return false;
    }
}
