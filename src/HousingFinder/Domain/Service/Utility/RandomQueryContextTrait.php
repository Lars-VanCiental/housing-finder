<?php

namespace HousingFinder\Domain\Service\Utility;

/**
 * Class RandomQueryContextTrait
 *
 * @package HousingFinder\Domain\Service\Utility
 */
trait RandomQueryContextTrait
{
    /**
     * @return resource
     */
    protected function getQueryContext()
    {
        return stream_context_create(
            [
                'http' => [
                    'method' => 'GET',
                    'header' => RandomUserAgent::getUserAgent(),
                ],
            ]
        );
    }
}
