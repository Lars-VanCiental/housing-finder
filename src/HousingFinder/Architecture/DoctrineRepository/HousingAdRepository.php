<?php

namespace HousingFinder\Architecture\DoctrineRepository;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use HousingFinder\Domain\Model\Entity\HousingAd;
use HousingFinder\Domain\Model\HousingAdRepositoryInterface;
use HousingFinder\Domain\Model\Identifier\HousingAdIdentifier;

/**
 * Class HousingAdRepository
 *
 * @package HousingFinder\Architecture\DoctrineRepository
 */
class HousingAdRepository extends EntityRepository implements HousingAdRepositoryInterface
{
    /**
     * @param \HousingFinder\Domain\Model\Identifier\HousingAdIdentifier $housingAdIdentifier
     * @return bool
     */
    public function exists(HousingAdIdentifier $housingAdIdentifier)
    {
        try {
            $this->get($housingAdIdentifier);

            return true;
        } catch (EntityNotFoundException $e) {
            return false;
        }
    }

    /**
     * @param HousingAd $housingAd
     */
    public function add(HousingAd $housingAd)
    {
        $this->getEntityManager()->persist($housingAd);
    }

    /**
     * @param HousingAd $housingAd
     */
    public function remove(HousingAd $housingAd)
    {
        $this->getEntityManager()->remove($housingAd);
    }

    /**
     * @param HousingAdIdentifier $housingAdIdentifier
     * @return HousingAd
     * @throws EntityNotFoundException
     */
    public function get(HousingAdIdentifier $housingAdIdentifier) : HousingAd
    {
        $housingAd = $this->findOneBy(
            [
                'identifier.sourceName' => $housingAdIdentifier->getSourceName(),
                'identifier.sourceIdentifier' => $housingAdIdentifier->getSourceIdentifier(),
            ]
        );

        if (null == $housingAd) {
            throw EntityNotFoundException::fromClassNameAndIdentifier(
                $this->getClassName(),
                [
                    $housingAdIdentifier->getSourceName(),
                    $housingAdIdentifier->getSourceIdentifier(),
                ]
            );
        }

        return $housingAd;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return HousingAd[]
     */
    public function findNew($limit = 10, $offset = 0) : array
    {
        return $this->findBy(
            [
                'status' => HousingAd::STATUS_NEW,
                'original' => null,
            ],
            [
                'updateDate' => 'asc',
            ],
            $limit,
            $offset
        );
    }

    /**
     * @param array $filters
     * @param int   $limit
     * @param int   $offset
     * @return mixed
     */
    public function findOnGoing(array $filters = [], $limit = 10, $offset = 0) : array
    {
        $queryBuilder = $this->createQueryBuilder('ha');
        $validOnGoingStatutes = [
            HousingAd::STATUS_ACCEPTED,
            HousingAd::STATUS_CONTACTED,
            HousingAd::STATUS_RESPONDED,
            HousingAd::STATUS_WAITING,
            HousingAd::STATUS_KEPT,
        ];

        $statusPriorityList = '(CASE';
        $priority = 0;
        foreach ($validOnGoingStatutes as $validOnGoingStatus) {
            $statusPriorityList .= ' WHEN ha.status = \''.$validOnGoingStatus.'\' THEN '.++$priority;
        }
        $statusPriorityList .= ' ELSE 0 END) AS HIDDEN sortValue ';

        $query = $queryBuilder
            ->addSelect($statusPriorityList)
            ->where(
                $queryBuilder->expr()->in('ha.status', $validOnGoingStatutes)
            )
            ->andWhere(
                $queryBuilder->expr()->isNull('ha.original')
            )
            ->addOrderBy('sortValue', 'ASC')
            ->addOrderBy('ha.surface', 'DESC')
            ->addOrderBy('ha.price', 'ASC')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->getQuery();

        return $query->getResult();
    }

    /**
     * @return array
     */
    public function findDuplicateTestFields() : array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $query = $queryBuilder
            ->select('CONCAT(ha.identifier.sourceName, CONCAT(\'_\', ha.identifier.sourceIdentifier)) AS identifier, ha.title, ha.description, ha.price, ha.surface, ha.numberOfRooms')
            ->from(HousingAd::class, 'ha')
            ->where(
                $queryBuilder->expr()->notIn(
                    'ha.status',
                    [HousingAd::STATUS_NEW]
                )
            )
            ->andWhere(
                $queryBuilder->expr()->isNull('ha.original')
            )
            ->getQuery();

        return $query->getResult();
    }
}
